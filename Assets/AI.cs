﻿using UnityEngine;
using System.Collections;

public class AI : MonoBehaviour 
{

	public GameObject[] waypoints;
	//14 saker

	public float tBetweenSpawns = 1.0f;
	public GameObject[] enemy;

	//time.time då spawn ska ske
	private float nextSpawnTime = 0f;

	private int enemySpawns = 0;

	void Start () 
	{
	}

	void Update () 
	{
		if(Time.time > nextSpawnTime)
		{
			nextSpawnTime = Time.time + tBetweenSpawns;

			if(enemySpawns != 9)
			{
				Instantiate(enemy[0], waypoints[0].transform.position , Quaternion.identity);
			}
			else
			{
				Instantiate(enemy[1], waypoints[0].transform.position , Quaternion.identity);
				enemySpawns = 0;
			}

			enemySpawns += 1;
		}
	}

}

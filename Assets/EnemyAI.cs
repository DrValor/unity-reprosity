﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour 
{
	public float Speed;

	private GameObject[] AIwaypoints;
	private int cTarget = 1;
	void Start ()
	{
		AIwaypoints = GameObject.Find("AI Control").GetComponent<AI>().waypoints;

	}

	void Move (int cTarget)
	{
		gameObject.transform.position = Vector3.MoveTowards(transform.position, AIwaypoints[cTarget].transform.position, Speed);
        
	}

	void Update () 
	{

		if(gameObject.transform.position == AIwaypoints[cTarget].transform.position)
		{
			cTarget ++;
			//Move (cTarget);
		}
		else
		{
			//move
			Move (cTarget);

		}

		if(gameObject.transform.position == AIwaypoints[AIwaypoints.Length - 1].transform.position)
		{
			//ta skada på spelaren


			//FÖRSTÖR
			Destroy(gameObject);
		}
			
	}
}
